package devcodenet.in.devroot;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;



import java.io.File;

public class RootDetection extends Application {

    public  static boolean isRooted()
    {
        try {
            for (String file : new String[]{"system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"}) {
                if (new File(file).exists()) {

                    return true;
                }
            }
        }catch (Exception ex)
        {

        }
        return false;
    }
}
