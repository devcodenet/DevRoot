package devcodenet.in.devroot;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import io.fabric.sdk.android.Fabric;

import static devcodenet.in.devroot.RootDetection.isRooted;

public class DevRoot extends AppCompatActivity {


    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        //setContentView(R.layout.activity_dev_root);

        MobileAds.initialize(this,"ca-app-pub-6462989004506783~5175368549");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-6462989004506783/2939567926");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                mInterstitialAd.isLoaded();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkroot();
    }

    public void checkroot() {
        if (RootDetection.isRooted()==true)
        {
                   showRootedDialogAction(this);
        }
        else
        {
                    showunRootedDialogAction(this);
        }
    }

    public  void showRootedDialogAction(final Context context) {
        try {
            if (RootDetection.isRooted() == true) {
                Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.rootview);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();


            }


        }catch (Exception ex)
        {

        }
    }
    public void showunRootedDialogAction(final Context context) {
        try {
            if (isRooted() == true) {
                Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.unrootview);
                dialog.show();
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }


        }catch (Exception ex)
        {

        }
    }

    public void exit(View view) {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            ActivityCompat.finishAffinity(this);
        }
        else {
            ActivityCompat.finishAffinity(this);
        }

    }
}
